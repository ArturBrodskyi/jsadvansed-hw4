function makeHttpRequest(method, url) {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();

        xhr.open(method, url)

        xhr.onload = () => {
            if (xhr.status >= 200) {
                resolve(xhr.responseText)
            } else {
                reject('Error')
            }
        }

        xhr.onerror = () => {
            reject('Error 2')
        };

        xhr.send();
    })
}


const url = 'https://ajax.test-danit.com/api/swapi/films';


makeHttpRequest('GET', url).then((data) =>{
    console.log(JSON.parse(data));
})
    .catch((err) =>{
        console.log(err);
    })

    fetch(url).then((data) => {
        return data.json();
    }).then((data) => {
        data.forEach(movie => {
            const ul = document.createElement('ul')
            document.body.appendChild(ul);
            const nameLi = document.createElement('li');
            nameLi.textContent = movie.name;
            ul.appendChild(nameLi);

            const episodeIdLi = document.createElement('li');
            episodeIdLi.textContent = movie.episodeId;
            ul.appendChild(episodeIdLi);

            const openingCrawlLi = document.createElement('li');
            openingCrawlLi.textContent = movie.openingCrawl;
            ul.appendChild(openingCrawlLi);

            movie.characters.forEach((characterURL) => {
                fetch(characterURL)
                    .then((charData) => charData.json())
                    .then((character) => {
                        const charachterLi = document.createElement('li');
                        charachterLi.textContent = character.name;
                        ul.appendChild(charachterLi);
                    })
                    .catch((err) => {
                        console.log(`Error fetching character: ${err}`);
                    });
            });

        });

    }).catch((err) => {
        console.log(err);
    });